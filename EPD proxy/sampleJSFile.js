function response(res, msg) {
    var body = JSON.parse(msg);
    if (body.msg_type == "reject") {
        res.status(400);
        res.send(body.text);
    }
    else {
        res.send(msg);
    }
}

function run() {
    var express = require('express');
    var parser  = require('body-parser');
    var app     = express();
    var epd     = require("bindings")("epd_proxy")
    var uuidv1  = require('uuid/v1');
    console.log("Starting...");
    process.env.path = process.env.path + ";lib/win64/";
    // need to update path for thridparty DLLs for epd
    try {
        epd.start("./epd_NF.json",
                  function (msg) {
                      console.log("Req: " + msg);
                  });
    }
    catch (err) {
        console.log(err);
        process.exit(1);
    }
    app.use(parser());
    console.log("hii... I am in run");
    app.listen(3000, function() {
        console.log('listening on :' + 3000);
    });
    app.get("/", function(req, res) {
    });
    app.get("/context", function(req, res) {
        var query = {
            "msg_type"   : "context",
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.get("/logon", function(req, res) {
        var query = {
            "msg_type"   : "logon",
            "user"       : req.query.user,
            "password"   : req.query.password,
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.post("/factor_list", function(req, res) {
        var query = {
            "msg_type"   : "factor_list",
            "command"    : "get",
            "vendor"     : req.body.vendor,
            "model"      : req.body.model,
            "currency"   : req.body.currency,
            "date"       : req.body.date,
            "ticker_list": req.body.ticker_list,
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.get("/factor_list_series", function(req, res) {
        var query = {
            "msg_type"   : "factor_list_series",
            "command"    : "get",
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.get("/sector_list", function(req, res) {
        var query = {
            "msg_type"   : "sector_list",
            "command"    : "get",
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.get("/industry_list", function(req, res) {
        var query = {
            "msg_type"   : "industry_list",
            "command"    : "get",
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.get("/basket_list", function(req, res) {
        var query = {
            "msg_type"   : "basket_list",
            "command"    : "get",
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.get("/basket", function(req, res) {
        var query = {
            "msg_type"      : "basket",
            "command"       : "get",
            "name"          : req.query.name,
            "classification": req.query.classification,
            "category"      : req.query.category,
            "date"          : req.query.date,
            "request_id"    : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.delete("/basket", function(req, res) {
        var query = {
            "msg_type"      : "basket",
            "command"       : "delete",
            "name"          : req.query.name,
            "classification": req.query.classification,
            "request_id"    : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.get("/strategy_list", function(req, res) {
        var query = {
            "msg_type"   : "strategy_list",
            "command"    : "get",
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.get("/strategy", function(req, res) {
        var query = {
            "msg_type"   : "strategy",
            "command"    : "get",
            "name"       : req.query.name,
            "date"       : req.query.date,
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.delete("/strategy", function(req, res) {
        var query = {
            "msg_type"   : "strategy",
            "command"    : "delete",
            "name"       : req.query.name,
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.get("/instrument/:ticker", function(req, res) {
        var query = {
            "msg_type"   : "instrument",
            "command"    : "get",
            "ticker"     : req.params.ticker,
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.get("/backtest_list", function(req, res) {
        var query = {
            "msg_type"   : "backtest_list",
            "command"    : "get",
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.delete("/backtest", function(req, res) {
        var query = {
            "msg_type"   : "backtest",
            "command"    : "delete",
            "name"       : req.query.name,
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.post("/fundamental", function(req, res) {
        var query = {
            "msg_type"   : "fundamental",
            "command"    : "get",
            "date"       : req.body.date,
            "ticker_list": req.body.ticker_list,
            "column_list": req.body.column_list
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.get("/fundamental_list", function(req, res) {
        var query = {
            "msg_type"   : "fundamental_list",
            "command"    : "get",
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.post("/timeseries", function(req, res) {
        var query = {
            "msg_type"   : "timeseries",
            "command"    : "get",
            "start_date" : req.body.start_date,
            "end_date"   : req.body.end_date,
            "ticker_list": req.body.ticker_list,
            "request_id" : uuidv1()
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.post("/allocation", function(req, res) {
        var query = {
            "msg_type"                 : "allocation",
            "command"                  : "get",
            "request_id"               : uuidv1(),
            "date"                     : req.body.date,
            "category_list"            : req.body.category_list,
            "basket_constraints"       : req.body.basket_constraints,
            "basket_view_constraints"  : req.body.basket_view_constraints,
            "country_constraints"      : req.body.country_constraints,
            "country_view_constraints" : req.body.country_view_constraints,
            "factor_constraints"       : req.body.factor_constraints,
            "factor_view_constraints"  : req.body.factor_view_constraints,
            "sector_constraints"       : req.body.sector_constraints,
            "sector_view_constraints"  : req.body.sector_view_constraints,
            "industry_constraints"     : req.body.industry_constraints,
            "industry_view_constraints": req.body.industry_view_constraints,
            "region_constraints"       : req.body.region_constraints,
            "region_view_constraints"  : req.body.region_view_constraints,
            "stock_constraints"        : req.body.stock_constraints,
            "stock_view_constraints"   : req.body.stock_view_constraints
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    app.post("/analytics", function(req, res) {
        var query = {
            "msg_type"                 : "analytics",
            "command"                  : "get",
            "request_id"               : uuidv1(),
            "date"                     : req.body.date,
            "category_list"            : req.body.category_list,
            "basket_list"              : req.body.basket_list,
            "factor_currency"          : req.body.factor_currency,
            "factor_date"              : req.body.factor_date,
            "factor_model"             : req.body.factor_model,
            "factor_vendor"            : req.body.factor_vendor,
            "stock_allocation"         : req.body.stock_allocation,
            "sector_allocation"        : req.body.sector_allocation,
            "industry_allocation"      : req.body.industry_allocation,
            "region_allocation"        : req.body.region_allocation,
            "country_allocation"       : req.body.country_allocation,
            "basket_allocation"        : req.body.basket_allocation
        };
        epd.process(JSON.stringify(query),
                    function (msg) {
                        response(res, msg);
                    });
    });
    console.log("Finished!");
}

run();
